var express = require('express')
var router = express.Router()
const jwt = require("jsonwebtoken");

// define the home page route
router.post('/login', function (req, res) {
    const token = generateAccessToken({username: req.body.username})
    res.cookie("auth", token)
    res.json(token);

})

function generateAccessToken(username) {
    // expires after half and hour (1800 seconds = 30 minutes)
    return jwt.sign(username, process.env.PRIVATE_JWT_KEY, { expiresIn: '1800s' });
  }

module.exports = router