var helpers = {}

helpers.getUsuarioDeJWT = function(jwt) {
    console.log("JWT: ", jwt)
}


helpers.okResponse = function(msg, data, res) {

    var estado = "OK"
    var statusCode = 200

    var datos = {
        "estado": estado,
        "mensaje": msg,
        "datos": data
    }

    res.status = statusCode

    res.send(datos);

}
helpers.errorResponse = function(msg, res) {

    var estado = "NO"
    var statusCode = 500


    var datos = {
        "estado": estado,
        "mensaje": msg
    }

    res.status = statusCode

    res.send(datos);

}

module.exports = helpers