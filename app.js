const express = require('express')
const app = express()
const cookieParser = require('cookie-parser')
const moment = require('moment'); 
const dotenv = require("dotenv");
const port = 3000
const { Sequelize } = require('sequelize');

app.use(express.json())
app.use(cookieParser());

var api = require('./api')
var session = require('./session')

dotenv.config();
moment.locale("es"); 

app.use('/api', api)
app.use('/session', session)


app.listen(port, () => {
  console.log(`APP running at: http://localhost:${port}`)
})