var express = require('express')
var moment = require('moment')
var crypto = require('crypto') 
moment.locale("es"); 
var router = express.Router()
const jwt = require("jsonwebtoken")
var helpers = require('./helpers')



router.use(function timeLog (req, res, next) {
  console.log("Endpoint: " + req.baseUrl + ' | Fecha: ', moment().format('MMMM Do YYYY, h:mm:ss a'));

  next()
})

router.all("*", function requireAuthentication(req, res, next) {


    if(req.cookies.auth !== undefined) {
        
        try {
            var decoded = jwt.verify(req.cookies.auth, process.env.PRIVATE_JWT_KEY);
            helpers.getUsuarioDeJWT(decoded);
            next()
          } catch(err) {
            console.log("JWT Error: " + err.message)
            helpers.errorResponse("Ocurrió un error al validar el JWT: " + err.message, res)
          }

    } else {
        helpers.errorResponse("Necesita estar autenticado.", res)
    }
})


router.get('/', function (req, res) {
    var info = {
        info: "algo",
        data: "Algo"
    }

  helpers.okResponse("OK", info, res)

})

module.exports = router